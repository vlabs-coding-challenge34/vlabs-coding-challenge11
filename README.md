<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Challenge 2</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <!-- Website Page header -->
    <header>
        <h1>Challenge 2</h1>
    </header>
    <!-- next line to header -->
    <p id="p1">*Instruction to perform </p>
    <section>
        <ul>
            <!-- Theory of the project -->
            <h3 align="center">Steps</h3><br>
            <li>Step 1 : Observe the image carefully .</li><br><br>
            <li>Step 2 : Click On the Image you want and Drag it on the Obsereved image.</li><br><br>
            <li>Step 3 : Drop the image on it .</li>
            <p id="p2"><b>*Images are given Below*</b></p>
        </ul>
    </section>

    <!-- Main Image -->
    <article>
        <img id="mainimg" src="main.png" alt="Main Image"><br>
        <hr>
        <div class="ImagesBox" draggable="true">
            <img src="9.png" id="A" alt="image">
            <img src="2.png" id="B" alt="image">
            <img src="3.png" id="C" alt="image">
            <img src="4.png" id="D" alt="image">
            <img src="5.png" id="E" alt="image">
            <img src="6.png" id="F" alt="image">
            <img src="7.png" id="G" alt="image">
            <img src="8.png" id="H" alt="image">
            <img src="1.png" id="I" alt="image"> 
        </div>
    </article>
    <footer style="text-align: center; font-size: xx-large;"><b>Thankyou!</b></footer>

</body>
<!-- <script src="index.js"></script> -->

</html>
